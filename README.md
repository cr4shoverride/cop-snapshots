Simple example app that should help to understand: 
- Components
- Component tests
- Snapshot tests

Installation instructions: 

- Download and install node: https://nodejs.org/en/download/
- Once installed, open the directory of the app in the terminal
- Run "npm install"
- To start the server and run the app, type: "npm start"
- To start the tests, run "npm test"

There are some example tests already setup, look for them in "registration_form.test.js".