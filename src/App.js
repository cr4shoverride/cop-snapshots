import React from 'react';
import RegistrationForm from './components/registration_form';

function App() {
  return <RegistrationForm />;
}

export default App;
