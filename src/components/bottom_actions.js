import React from 'react';
import './bottom_actions.css';

function BottomActions({ primaryActionTitle, onClickPrimaryAction, disabled }) {
  return (
    <div className="bottom_actions">
      <button onClick={onClickPrimaryAction} disabled={disabled}>
        {primaryActionTitle}
      </button>
      <button>Cancel</button>
    </div>
  );
}

export default BottomActions;
