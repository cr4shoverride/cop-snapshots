import React from 'react';
import { shallow, mount } from 'enzyme';
import BottomActions from './bottom_actions';

describe('Bottom actions', () => {
  it('renders shallow', () => {
    const component = shallow(<BottomActions primaryActionTitle="Login" />);
    expect(component).toMatchSnapshot();
  });
});
