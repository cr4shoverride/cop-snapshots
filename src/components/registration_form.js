import React from 'react';
import Header from './header';
import BottomActions from './bottom_actions';
import './registration_form.css';

class RegistrationForm extends React.Component {
  state = {
    email: '',
    password: '',
    isLoggedIn: false,
  };

  handleClickRegister = () => {
    this.setState({ isLoggedIn: true });
  };

  handleChangeEmail = event => {
    this.setState({ email: event.target.value });
  };

  handleChangePassword = event => {
    this.setState({ password: event.target.value });
  };

  renderInputFields = () => {
    const { email, password } = this.state;

    return (
      <div className="input_fields">
        <input
          placeholder="Email"
          onChange={this.handleChangeEmail}
          data-name="email"
          value={email}
        />
        {!isValidEmail(email) && <label>Email not valid</label>}
        <input
          placeholder="Password"
          type="password"
          onChange={this.handleChangePassword}
          data-name="password"
          value={password}
        />
        {!isValidPassword(password) && <label>Password not valid</label>}
      </div>
    );
  };

  render() {
    const { email, password, isLoggedIn } = this.state;

    return (
      <div className="registragion_form_container">
        {isLoggedIn ? (
          <div>Logged in!</div>
        ) : (
          <div className="registration_form">
            <Header title="You can register here!" />
            {this.renderInputFields()}
            <BottomActions
              primaryActionTitle="Register"
              onClickPrimaryAction={this.handleClickRegister}
              disabled={!isValidEmail(email) || !isValidPassword(password)}
            />
          </div>
        )}
      </div>
    );
  }
}

export function isValidEmail(email) {
  const emailRegEx = /\S+@\S+\.\S+/;
  return emailRegEx.test(email);
}

export function isValidPassword(password) {
  return password.length > 6;
}

export default RegistrationForm;
