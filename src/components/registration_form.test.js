import React from 'react';
import { shallow, mount } from 'enzyme';
import RegistrationForm, { isValidPassword } from './registration_form';

describe('Registration form', () => {
  it('renders shallow', () => {
    const component = shallow(<RegistrationForm />); // Renders a shallow version of the compoenent (doesn't render child components)
    expect(component).toMatchSnapshot();
  });

  it('does stuff ', () => {
    const component = mount(<RegistrationForm />); // mount will render child components as well

    const inputFields = component.find('input_fields'); // find elements by css className

    const passwordInput = component.find('[data-name="password"]'); // find by dedicated selector

    console.log(passwordInput.debug()); // Log debug output of element to console

    const buttons = component.find('button'); // Gets everything that is a button

    expect(buttons).toHaveLength(2); // Evaluate condition - check if we have two buttons

    // More on https://jestjs.io/docs/en/expect

    component.setState({ email: 'valid@email', password: 'validpassword' }); // Update the component state (easier than setting value of input fields directly)

    buttons.at(0).simulate('click'); // Click on the first button

    expect(component).toMatchSnapshot(); // See if we render the correct stuff after clicking the button

    // Run the tests with the command "npm test"
  });

  it('does more stuff', () => {
    const component = shallow(<RegistrationForm />);

    // Add more tests
  });

  // Example of a unit test validating a single function (vs component test above)
  it('validates password correctly', () => {
    const validPassword = 'longerThan6Letters';
    expect(isValidPassword(validPassword)).toBeTruthy();
  });
});
